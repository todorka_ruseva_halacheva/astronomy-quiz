import React, { useState, useEffect, useCallback } from 'react';
import Start from '../start';
import Question from '../question';
import Result from '../result';
import ProgressBar from '../progressBar';
import Timer from '../timer';
import './styles.css';

const MAX_TIME = 10;
const questions = [
  {
    text: 'How many planets are there in our Solar System?',
    answers: [
      {
        text: '8',
        correct: true,
      },
      { text: '9' },
      { text: '10' },
    ],
  },
  {
    text: 'What is the 3rd planet from the Sun?',
    answers: [
      {
        text: 'Earth',
        correct: true,
      },
      { text: 'Mars' },
      { text: 'Venus' },
    ],
  },
  {
    text: 'How old is the Earth in years?',
    answers: [
      {
        text: '4.5 billion',
        correct: true,
      },
      { text: '6 billion' },
      { text: '3 billion' },
    ],
  },
  {
    text: 'What are shooting stars?',
    answers: [
      {
        text: 'Meteors',
        correct: true,
      },
      { text: 'Comets' },
      { text: 'Satellites' },
    ],
  },
  {
    text: 'Who was the first human to step on the Moon?',
    answers: [
      {
        text: 'Niel Armstrong',
        correct: true,
      },
      { text: 'Buzz Aldrin' },
      { text: 'Yuri Gagarin' },
    ],
  },
  {
    text: 'What do we call the deep holes on the Moon?',
    answers: [
      {
        text: 'Craters',
        correct: true,
      },
      { text: 'Pits' },
      { text: 'Cavities' },
    ],
  },
  {
    text: 'What is the name of our galaxy?',
    answers: [
      {
        text: 'Milky Way',
        correct: true,
      },
      { text: 'Andromeda' },
      { text: 'Twix' },
    ],
  },
];

export default function QuizApp() {
  const [current, setCurrent] = useState(-1);
  const [totalTime, setTotalTime] = useState(0);
  const [score, setScore] = useState(0);

  const [seconds, setSeconds] = useState(0);
  const [isPlaying, setIsPlaying] = useState(false);

  const stop = useCallback(answer => {
    setTotalTime(totalTime + seconds);
    setIsPlaying(false);

    if (answer && answer.correct) {
      setScore(score + 1);
    }
  }, [score, seconds, totalTime]);


  useEffect(() => {
    let timeout = null;

    if (current >= 0 && current < questions.length && !isPlaying) {
      timeout = setTimeout(() => {
        setSeconds(0);
        setCurrent(current + 1);
        setIsPlaying(true);
      }, 2000);
    } else {
      clearTimeout(timeout);
    }

    return () => clearTimeout(timeout);
  }, [current, isPlaying]);

  useEffect(() => {
    let interval = null;

    if (isPlaying) {
      interval = setInterval(() => {
        if (seconds < MAX_TIME) {
          setSeconds(seconds => seconds + 1);
        } else {
          stop();
        }
      }, 1000);
    } else {
      clearInterval(interval);
    }

    return () => clearInterval(interval);
  }, [isPlaying, seconds, stop]);

  const startQuiz = () => {
    setCurrent(0);
    setIsPlaying(true);
  }

  const renderContent = () => {
    if (current < 0) {
      return <Start startQuiz={startQuiz} />;
    }

    if (current >= questions.length) {
      return <Result score={score} total={questions.length} time={totalTime} ended />;
    }

    return (
      <div className="play-container">
        <Timer seconds={seconds} total={MAX_TIME} />
        <Result score={score} total={questions.length} time={totalTime} />
        <Question question={questions[current]} isFreezed={!isPlaying} onAnswer={stop} />
        <ProgressBar current={current + 1} total={questions.length} />
      </div>
    );
  }

  return (
    <div className="quiz-app">
      {renderContent()}
    </div>
  );
}
