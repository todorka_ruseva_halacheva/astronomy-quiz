import React, { useState, useEffect } from 'react';
import './styles.css';

export default function Answer({ answer, isFreezed, isSelected, onClick }) {
  const [showCorrect, setShowCorrect] = useState(false);

  useEffect(() => {
    let timeout = null;

    if (isFreezed) {
      timeout = setTimeout(() => {
        setShowCorrect(true);
      }, 200);
    } else {
      setShowCorrect(false);
      clearTimeout(timeout);
    }

    return () => clearTimeout(timeout);
  }, [isFreezed]);

  const highlightSelected = () => {
    return isSelected ? ' highlight' : '';
  }

  const highlightCorrect = () => {
    if (showCorrect) {
      if (isSelected) {
        return answer.correct ? ' blink' : ' dark';
      } else if (answer.correct) {
        return ' highlight';
      }
    }

    return '';
  }

  return (
    <button
      disabled={isFreezed}
      className={'button answer' + highlightSelected() + highlightCorrect()}
      style={{

      }}
      onClick={() => onClick(answer)}>
        {answer.text}
    </button>
  );
}
