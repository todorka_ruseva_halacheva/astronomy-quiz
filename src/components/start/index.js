import React from 'react';
import './styles.css';

export default function Start({ startQuiz }) {
  return (
    <div className="start-container">
      <button className="button highlight uppercase" onClick={startQuiz}>Play</button>
    </div>
  );
}
