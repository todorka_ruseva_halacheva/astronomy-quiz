import React from 'react';
import './styles.css';

export default function Result({ score, total, time, ended }) {
  return (
    <div className={'result-container' + (ended ? ' ended' : '')}>
      {ended ? (
        <div>
          <div className="score">{score} / {total}</div>
          <div className="time">{time}s</div>
        </div>
        ) :
        (
          <div className="score">{score}</div>
        )
      }
    </div>
  );
}
