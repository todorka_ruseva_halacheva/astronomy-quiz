import React, { useState } from 'react';
import './styles.css';
import Answer from '../answer';

export default function Question({ question, onAnswer, isFreezed }) {
  const [selectedAnswer, setSelectedAnswer] = useState(undefined);

  const onClick = answer => {
    setSelectedAnswer(answer);
    onAnswer(answer);
  }

  return (
    <div className="question-container">
      <div className="question">{question.text}</div>
      {question.answers.map(answer =>
        <Answer
          key={answer.text}
          answer={answer}
          isFreezed={isFreezed}
          isSelected={selectedAnswer && selectedAnswer.text === answer.text}
          onClick={onClick}
        />
      )}
    </div>
  );
}
