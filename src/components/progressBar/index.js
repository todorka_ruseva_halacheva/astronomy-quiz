import React from 'react';
import './styles.css';

export default function ProgressBar({ current, total }) {
  const width = current / total * 100 + '%';

  return (
    <div className="progress-bar-container">
      <div className="current">{current} / {total}</div>
      <div className="progress-bar">
        <div className="progress" style={{ width: width }} />
      </div>
    </div>
  );
}
