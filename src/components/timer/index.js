import React from 'react';
import './styles.css';

export default function Timer({ seconds, total }) {
  const progress = (1 - seconds / total) * 157;

  return (
    <div className="timer-container">
      <div className="seconds">{seconds}s</div>
      <svg width="54" height="54" viewBox="0 0 54 54">
        <circle r="25" cx="27" cy="27" className="background"></circle>
        <circle r="25" cx="27" cy="27" className="progress" style={{ strokeDashoffset: progress}}></circle>
      </svg>
    </div>
  );
}
